package main

import (
	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
	"golang.org/x/oauth2"
)

func auth(prop properties) (client *twitter.Client) {

	config := oauth1.NewConfig(prop.keyValMap[consumerKey], prop.keyValMap[consumerSecret])
	token := oauth1.NewToken(prop.keyValMap[accessToken], prop.keyValMap[accessSecret])

	httpClient := config.Client(oauth2.NoContext, token)
	client = twitter.NewClient(httpClient)
	return
}
