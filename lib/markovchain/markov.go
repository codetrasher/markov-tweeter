package markovchain

import (
	"math/rand"
	"regexp"
	"strings"
)

var (
	//stateMap is a map with state as key pointing to values (suffixes).
	stateMap           map[state][]string
	prefix, start, end state
)

const tweetCharCount int = 140

type state struct {
	pref [2]string
}

/*
Init method initializes internal variables for use.
*/
func Init() {
	stateMap = make(map[state][]string, 0)
	// start, end = state{pref: [2]string{"start", ""}}, state{pref: [2]string{"end", ""}}
}

/*
Add method splits given string into separate words and builds the map containing
prefixes as keys and suffixes as values. State parameter determines the length
for prefix.
*/
func Add(phrase string) {

	// fmt.Printf("\n\nInitial phrase: %s\n\n", phrase)

	words := strings.Split(phrase, " ")

	// fmt.Printf("len(words): %d\n", len(words))

	for i, w := range words {

		// fmt.Printf("index: %d\n", i)
		if len(words) > 2 {
			// fmt.Printf("words: %v\n", words)
			// fmt.Printf("w: \"%s\", words[i+1]: \"%s\", words[i+2]: \"%s\"\n", w, words[i+1], words[i+2])
			prefix := state{pref: [2]string{w, words[i+1]}}
			suffix := stateMap[prefix]
			suffix = append(suffix, words[i+2])

			stateMap[prefix] = suffix
			if i+2 == len(words)-1 {
				break
			}
		}
	}
}

func GenerateTweet() (tweet string) {

	// fmt.Printf("stateMap:\n%v\n\n", stateMap)

	mapLength := len(stateMap)
	lastPrefix := state{}
	reg := regexp.MustCompile("^[A-Z]{1}")

	// Choose random index from 0..len(map)
	randindex := rand.Intn(mapLength - 1)
	i := 0

	/*
		Get the initial starting words on first iteration, build the lastPrefix and
		continue iteration.
	*/

	// Loop until i equals randindex
	exit := false
	for !exit {
		if i == randindex {
			for prefix, suffix := range stateMap {

				// fmt.Printf("suffix: %v\n", suffix)

				randIndex2 := 0
				if len(suffix) > 1 {
					randIndex2 = rand.Intn(len(suffix) - 1)
				}

				if reg.MatchString(prefix.pref[0]) {
					// Add new words to the tweet string
					tweet += prefix.pref[0] + " " + prefix.pref[1] + " " + suffix[randIndex2]

					// Assign values to lastPrefix
					lastPrefix.pref = [2]string{prefix.pref[1], suffix[randIndex2]}
					exit = true
					break

				} else {
					i = 0
					randindex = rand.Intn(mapLength - 1)
					break
				}

			}
		}
		if exit {
			break
		}
		i++
	}

	/*
		Rest of the tweet
	*/

	// Do until character count is about 140
	for len(tweet) <= 140 {

		// fmt.Printf("gen tweet: %s, len = %d\n", tweet, len(tweet))

		// If suffix has more than one item, randomize index, otherwise use zero.
		suffix := stateMap[lastPrefix]
		randIndex2 := 0
		if len(suffix) > 1 {
			randIndex2 = rand.Intn(len(suffix) - 1)
		} else {
			randIndex2 = 0
		}

		// fmt.Printf("suffix: %v\nsuffix len: %d\nrandIndex2 = %d\n\n", suffix, len(suffix), randIndex2)
		if len(suffix) == 0 || suffix[0] == "" || len(tweet+" "+suffix[randIndex2]) > 140 {
			break
		}

		tweet += " " + suffix[randIndex2]

		// Update lastPrefix
		lastPrefix.pref[0], lastPrefix.pref[1] = lastPrefix.pref[1], suffix[randIndex2]
	}

	// There can still be viable option in the suffix array for the last word.
	// Iterate through it and check if a smaller word will still fit.
	suffix := stateMap[lastPrefix]
	if len(suffix) > 0 {
		for j := 0; j < len(suffix); j++ {
			if len(tweet+" "+suffix[j]+".") <= 140 && suffix[j] != "" {
				tweet += " " + suffix[j] + "."
			}
		}
	}

	// Finally, add period if len is still below 140 and there's no dot as a
	// last character.
	r := regexp.MustCompile("[.!?]+\\s*$")
	if len(tweet) < 140 && !r.MatchString(tweet) {
		strings.TrimRight(tweet, " ")
		tweet += "."
	}

	return
}
