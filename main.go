package main

import (
	"fmt"
	"log"
	"os"

	mc "bitbucket.org/codetrasher/markov-tweeter/lib/markovchain"
	kp "github.com/alecthomas/kingpin"
)

const propfile string = "app.properties"
const consumerKey string = "consumerKey"
const consumerSecret string = "consumerSecret"
const accessToken string = "accessToken"
const accessSecret string = "accessSecret"

var (
	app    = kp.New("markov-tweeter", "A tweet generator powered by Markov chain algorithm.")
	parse  = app.Command("parse", "Trigger parsing command.")
	twUser = parse.Arg("user", "Twitter username.").Required().String()
)

func main() {
	// testphrase := "The failing @nytimes has disgraced the media world. Gotten me wrong for two solid years. Change libel laws?"
	// mc.Init()
	// mc.Add(testphrase)

	p, err := read(propfile)
	if err != nil {
		log.Fatalf("Application error: %s\n", err.Error())
	}
	fmt.Printf("app.properties:\n%v\n", p)

	// Authenticate application
	client := auth(p)
	// user, resp, err := client.Users.Show(&twitter.UserShowParams{
	// 	ScreenName: "NotRealDTBot",
	// })
	// fmt.Printf("user: %v\nresp: %v\nerr: %v\n\n", user, resp, err)

	switch kp.MustParse(app.Parse(os.Args[1:])) {
	case parse.FullCommand():

		// Init Markov chain
		mc.Init()

		var tweets []string
		if ok, file := checkAlreadyParsed(twUser); ok {
			tweets = parseFromFile(file)
		} else {
			tweets = parseTimeline(twUser, client)
		}

		// fmt.Printf("tweets:\n%v\n\n", tweets)
		// fmt.Printf("total: %d\n", len(tweets))

		for _, t := range tweets {
			mc.Add(t)
		}

		tweet := mc.GenerateTweet()

		fmt.Println()
		fmt.Println("=====================")
		fmt.Printf("Generated tweet:\n%s\n", tweet)
		fmt.Printf("length: %d characters\n\n", len(tweet))
	}
}
