package main

import (
	"bufio"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/dghubble/go-twitter/twitter"
)

var (
	tweetArchive, tweets []twitter.Tweet
	resp                 *http.Response
	err                  error
	user                 string
	includeRT            bool
)

func parseTimeline(username *string, client *twitter.Client) (tweetsStr []string) {

	user = *username
	includeRT = false

	log.Printf("parse timeline called for username: %s\n", *username)

	// Set up a ticker to request tweets on a certain interval. Interval = 1 s
	tickChan := time.NewTicker(time.Millisecond * 1000).C

	// Set up finishing timer for request loop
	doneChan := make(chan bool)
	doneLoop := false
	go func() {
		time.Sleep(time.Second * 16)
		doneChan <- true
		doneLoop = true
	}()

	tweetCount := 0

	// For-loop and a select-clause to handle concurrency
	maxID := int64(-1)
	for {
		select {
		case <-tickChan:
			if maxID == -1 {
				tweets, resp, err = client.Timelines.UserTimeline(&twitter.UserTimelineParams{
					ScreenName:      user,
					Count:           200,
					IncludeRetweets: &includeRT,
				})

			} else {
				tweets, resp, err = client.Timelines.UserTimeline(&twitter.UserTimelineParams{
					ScreenName:      user,
					Count:           200,
					MaxID:           maxID,
					IncludeRetweets: &includeRT,
				})
			}

			// If no more tweets are found, end the request loop
			if len(tweets) == 0 {
				doneChan <- true
				tickChan = nil
				// doneLoop = true
				break
			}

			tweetCount += len(tweets)

			// Get maxId for the next request
			if len(tweets) > 0 {
				maxID = tweets[len(tweets)-1].ID - 1
			}

			if err == nil && resp.StatusCode == 200 {

				log.Println("Tweets request success")
			}

			// Push tweets to tweetArchive
			tweetArchive = append(tweetArchive, tweets...)
		case <-doneChan:
			log.Println("Request loop done.")
			doneLoop = true
			break
		}

		if doneLoop {
			break
		}
	}

	// Concurrent sanitizing method to remove extra newlines from tweets. Append
	// sanitized tweets to the tweetsStr array while sanitizing.
	done := make(chan bool)
	go func() {

		for _, t := range tweetArchive {
			text := strings.Replace(t.Text, "\n", " ", -1)

			// TODO: Remove http links from tweets.
			removeLinks(&text)

			tweetsStr = append(tweetsStr, text)
		}

		done <- true
	}()
	<-done

	// Store tweets locally
	storeTweets(tweetsStr)

	return
}

func parseFromFile(f *os.File) (tweetsText []string) {

	log.Println("Parsing from file.")

	// Read the file
	scanner := bufio.NewScanner(f)
	lastLine := ""
	for scanner.Scan() {
		line := scanner.Text()

		removeLinks(&line)

		lastLine += " " + line

		if line == "" || line == "\n" {
			tweetsText = append(tweetsText, lastLine)
			lastLine = ""
			continue
		}

	}

	log.Printf("Parsing from file finished.\n")

	// fmt.Printf("tweetsText:\n%v\n\n", tweetsText)

	return
}

func removeLinks(text *string) {

	r := regexp.MustCompile("(http|https)\\:\\/\\/[a-zA-Z.\\/0-9]+")
	if r.MatchString(*text) {
		// fmt.Printf("text: %s\n", *text)
		*text = r.ReplaceAllString(*text, "")
		// fmt.Printf("text after replace: %s\n\n", *text)
	}
}

func storeTweets(tweetsStr []string) {
	f, _ := os.Create("tweets_by_" + user)
	w := bufio.NewWriter(f)
	defer w.Flush()

	for _, t := range tweetsStr {
		w.WriteString(t + "\n\n")
	}

}

/*
Checks if there alrady are tweets from specific user
*/
func checkAlreadyParsed(username *string) (isParsed bool, file *os.File) {
	if _, err := os.Stat("tweets_by_" + *username); err == nil {
		file, err = os.Open("tweets_by_" + *username)
		return true, file
	}
	return false, nil
}
