package main

import (
	"bufio"
	"os"
	"strings"
)

type properties struct {
	keyValMap map[string]string
}

/*
Read the properties file and assign the key-value pairs into properties object.
*/
func read(propfile string) (properties, error) {

	// Create and initialize a properties object
	p := properties{
		keyValMap: make(map[string]string, 0),
	}

	// If an error occurs, return empty properties and the error
	dir, err := os.Getwd()
	if err != nil {
		return p, err
	}

	// Try to open the file for reading
	f, err := os.Open(dir + "/" + propfile)
	if err != nil {
		return p, err
	}
	defer f.Close()

	// Read the file
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()

		if line == "" {
			continue
		}

		if !strings.HasPrefix(line, "# ") { // Read line unless it's empty or commented lines
			vals := strings.Split(line, "=")
			k, v := vals[0], vals[1]
			p.keyValMap[k] = v
		}
	}

	return p, nil
}
